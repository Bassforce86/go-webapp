# Welcome to the Repo!
This is designed and built to work on ubuntu / linux.

## Pre-requisites
* A working Docker installation
* A working GO installation

 > If your docker installation requires sudo, you'll probably need to change some stuff...

## How to use
This is a self contained project.
once pulled, run:

`cd src/main`

`./docker-build.sh`

## Things to note:
This is primarily being built as a hobby project to see how small and how easily a working development environment can be created for a webapp built in GO

As such, recommendations are always welcome, but likely all pull requests will be denied.
This is open-source because I feel it should be, and if it gains any significant following I'll ammend this policy.

## The Plan
Primarily, this is going to be base / foundation for a web framework in GO.
I'd like to try and bring a Rails-esque feel to GO, hopefully without the huge bloat overhead.

### Maintainers
* James King <bassbase.jk@gmail.com> 
