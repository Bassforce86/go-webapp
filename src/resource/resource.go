package resources

import (
	"fmt"
	"io/ioutil"
	"log"
)

type Resource struct {
	Name string
	File []byte
}

func Fetch(filename string, ext string) (*Resource, error) {
	file := fmt.Sprintf("resources/%s/%s.%s", ext, filename, ext)
	resource, err := ioutil.ReadFile(file)
	if err != nil {
		log.Printf("Couldn't gather resource: %s", filename)
		return nil, err
	}
	return &Resource{Name: filename, File: resource}, nil
}