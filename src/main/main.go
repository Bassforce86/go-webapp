package main

import (
	"log"
	"net/http"
	"os"
	"db"
)

type Environment struct {
	Name string
	Port string
}

func configureEnvironment() Environment {
	env := os.Getenv("env")
	if env == "" {
		env = "local"
	}
	port := os.Getenv("port")
	if port == "" {
		port = "8080"
	}

	return Environment{Name: env, Port: port}
}

func main() {
	env := configureEnvironment()
	log.Printf("Application Environment: %s", env.Name)
	log.Printf("Application starting up on port: %s", env.Port)
	db.Setup(env.Name)
	http.HandleFunc("/create/", PageHandler(CreateHandler))
	http.HandleFunc("/edit/", PageHandler(EditHandler))
	http.HandleFunc("/save/", PageHandler(SaveHandler))
	http.HandleFunc("/delete/", PageHandler(DeleteHandler))
	http.HandleFunc("/resources/", ResourceHandler(renderResource))
	http.HandleFunc("/", PageHandler(ViewHandler))
	log.Fatal(http.ListenAndServe(":"+env.Port, nil))
}
