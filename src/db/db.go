package db

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

type Database struct {
	Db Details `yaml:"db"`
}

type Details struct {
	Host     string `yaml:"host"`
	Port     int16  `yaml:"port"`
	User     string `yaml:"username"`
	Password string `yaml:"password"`
	Name     string `yaml:"name"`
}

var ConnectionString string

func Setup(environment string) {
	name := "environments/" + environment + ".yaml"
	config, err := os.Open(name)
	if err != nil {
		log.Println(err)
		return
	}
	defer config.Close()

	b, _ := ioutil.ReadAll(config)
	var env Database
	err = yaml.Unmarshal([]byte(b), &env)
	if err != nil {
		log.Println(err)
		return
	}

	conf := &env.Db

	log.Printf("Database: -host [%s] -port [:%d]", conf.Host, conf.Port)

	ConnectionString = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		conf.Host, conf.Port, conf.User, conf.Password, conf.Name)
}
