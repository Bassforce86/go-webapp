#!/usr/bin/env bash

# Create Docker Development Network
NETWORK_NAME=development
if [ -z $(docker network ls --filter name=^${NETWORK_NAME}$ --format="{{ .Name }}") ] ; then
     docker network create ${NETWORK_NAME} ;
fi

# Create Postgres Image
docker container rm db --force
docker run -it --name db --restart always \
-e POSTGRES_USER=webapp \
-e POSTGRES_PASSWORD=mysecretpassword \
-e PG_TRUST_LOCALNET=true \
-d --network development -p 5432:5432 postgres;

# Create GO binary
if [ ! -s main ]; then
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
    docker build -f Dockerfile -t webapp .
fi
docker container rm webapp --force
docker run -it --name webapp --network development -d -p 80:80 webapp

cd scripts
./wait-for-it.sh db:5432 -t 2 -q
cd ../
# Run db migrations
docker run --network development -v "$(pwd)"/db/migrations:/migrations migrate/migrate \
-path=/migrations/ \
-database postgres://webapp:mysecretpassword@db:5432/webapp?sslmode=disable up;

rm -f main