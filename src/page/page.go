package pages

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"main/db"
	"sort"
	"strings"
	"time"
)

var timeFormat = "15:04 | 02 Jan 2006 [GMT]"

type Page struct {
	PageId    int64     `json:"page_id" db:"page_id"`
	Name      string    `json:"title" db:"title"`
	Body      string    `json:"body" db:"body"`
	Parent    int64     `json:"parent" db:"parent"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
}

func (p *Page) CreatedOn() string {
	return p.CreatedAt.Format(timeFormat)
}
func (p *Page) UpdatedOn() string {
	return p.UpdatedAt.Format(timeFormat)
}
func (p *Page) Title() string {
	return strings.Title(p.Name)
}

func (p *Page) SavePage() error {
	log.Printf("page: attempting to set parent to '%d'", p.Parent)
	log.Printf("page: attempting to set title to '%s'", p.Name)
	log.Printf("page: attempting to set body to '%s'", p.Body)
	db, err := sql.Open("postgres", db.ConnectionString)
	if err != nil {
		log.Printf("db: connection failed - %s", err.Error())
		return err
	}
	fetch, err := FetchPage(p.PageId)
	if err != nil {
		log.Printf("page: cannot find '%s'", err.Error())
		_, err = create(db, p.Parent)
		if err != nil {
			return err
		}
		log.Print("page: save successful")
		return nil
	} else {
		err = update(db, fetch, p)
		if err != nil {
			return err
		}
		log.Print("page: save successful")
		return nil
	}

}
func (p *Page) DeletePage() error {
	db, err := sql.Open("postgres", db.ConnectionString)
	if err != nil {
		log.Print(err)
		return err
	}
	defer db.Close()
	err = remove(db, p)
	if err != nil {
		log.Print(err)
		return err
	}
	return nil
}
func (p *Page) Parents() []*Page {
	return Parents()
}
func (p *Page) Children() []*Page {
	return Children(p.PageId)
}
func (p *Page) Breadcrumb() []*Page {
	return Breadcrumb(p.PageId)
}

func Parents() []*Page {
	db, err := sql.Open("postgres", db.ConnectionString)
	if err != nil {
		log.Print(err)
		return nil
	}
	defer db.Close()
	rows, err := db.Query("select * from pages where parent=0")
	if err != nil {
		log.Print(err)
		return nil
	}
	pages := []*Page{}
	for rows.Next() {
		var page Page
		rows.Scan(&page.PageId,
			&page.Name,
			&page.Body,
			&page.CreatedAt,
			&page.UpdatedAt,
			&page.Parent)
		pages = append(pages, &page)
	}
	sort.Slice(pages, func(i, j int) bool {
		return pages[i].PageId < pages[j].PageId
	})

	return pages
}
func Children(parent int64) []*Page {
	db, err := sql.Open("postgres", db.ConnectionString)
	if err != nil {
		log.Print(err)
		return nil
	}
	defer db.Close()
	rows, err := db.Query("select * from pages where parent = $1", parent)
	if err != nil {
		log.Print(err)
		return nil
	}
	pages := []*Page{}
	for rows.Next() {
		var page Page
		rows.Scan(&page.PageId,
			&page.Name,
			&page.Body,
			&page.CreatedAt,
			&page.UpdatedAt,
			&page.Parent)
		pages = append(pages, &page)
	}
	sort.Slice(pages, func(i, j int) bool {
		return pages[i].PageId < pages[j].PageId
	})

	return pages
}
func Breadcrumb(id int64) []*Page {
	db, err := sql.Open("postgres", db.ConnectionString)
	if err != nil {
		log.Print(err)
		return nil
	}
	defer db.Close()
	query := "WITH RECURSIVE breadcrumb AS (" +
		"SELECT page_id, parent, title FROM pages where page_id = $1 " +
		"UNION ALL " +
		"SELECT p.page_id, p.parent, p.title FROM pages p " +
		"INNER JOIN breadcrumb b ON b.parent = p.page_id " +
		") SELECT * FROM breadcrumb ORDER BY page_id ASC;"
	rows, err := db.Query(query, id)
	if err != nil {
		log.Print(err)
		return nil
	}
	pages := []*Page{}
	for rows.Next() {
		var page Page
		rows.Scan(&page.PageId,
			&page.Parent,
			&page.Name)
		pages = append(pages, &page)
	}
	return pages
}

func CreatePage(current int64) (*Page, error) {
	db, err := sql.Open("postgres", db.ConnectionString)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	defer db.Close()

	page, err := create(db, current)
	if err != nil {
		log.Printf("page: err: %v", err)
		return nil, fmt.Errorf("page: no rows returned or created")
	}
	return page, nil
}
func FetchPage(id int64) (*Page, error) {
	var p Page
	db, err := sql.Open("postgres", db.ConnectionString)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	defer db.Close()
	row := db.QueryRow("select * from pages where page_id=$1", id)
	switch err := row.Scan(&p.PageId, &p.Name, &p.Body, &p.CreatedAt, &p.UpdatedAt, &p.Parent); err {
	case sql.ErrNoRows:
		return CreatePage(id)
	case nil:
		return &p, nil
	default:
		return nil, err
	}
}

func create(db *sql.DB, parent int64) (*Page, error) {
	page := new(Page)
	page.Name = "Page"
	page.Body = "New Page Text"
	page.Parent = parent
	page.CreatedAt = time.Now()
	page.UpdatedAt = time.Now()
	id := 0
	err := db.QueryRow("INSERT INTO pages (title, body, created_at, updated_at, parent) VALUES ($1, $2, $3, $4, $5) RETURNING page_id", page.Name, page.Body, time.Now(), time.Now(), parent).Scan(&id)
	if err != nil {
		log.Printf("page: create failed - %s", err.Error())
		return nil, err
	}
	page.PageId = int64(id)
	return page, nil
}
func update(db *sql.DB, from *Page, to *Page) error {
	statement := "UPDATE pages SET title = $2, body = $3, updated_at = $4 WHERE page_id = $1"
	_, err := db.Exec(statement, from.PageId, to.Name, to.Body, time.Now())
	if err != nil {
		log.Printf("page: update failed - %s", err.Error())
		return err
	}
	return nil
}
func remove(db *sql.DB, p *Page) error {
	// Remove children of this page first
	_, err := db.Exec("DELETE FROM pages WHERE parent = $1", p.PageId)
	if err != nil {
		log.Printf("page: child removal failed - %s", err.Error())
		return err
	}
	// Remove this page
	_, err = db.Exec("DELETE FROM pages WHERE page_id = $1", p.PageId)
	if err != nil {
		log.Printf("page: removal failed - %s", err.Error())
		return err
	}
	return nil
}
