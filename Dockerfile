FROM scratch
MAINTAINER James King <bassbase.jk@gmail.com>
ADD main /
COPY templates /templates
COPY resources /resources
COPY resources/css /resources/css
COPY resources/ico /resources/ico
COPY environments /environments
ENV port 80
ENV env test
CMD ["/main"]